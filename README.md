# virtual environment installation
pip install virtualenv

virtualenv sfenv

#### The following scripts should be run every time you first open the file
```bash
# MacOS
source sfenv/bin/activate

# Windows
mypthon\Scripts\activate
```
pip3 install -r requirements.txt

