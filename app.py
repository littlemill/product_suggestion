from flask import Flask, request, render_template, session
import pandas as pd
import numpy as np

app = Flask(__name__)

def user_factor_normalization(max,user_factor):
  normalized_user_factor = dict()
  for factor in user_factor.keys():
    normalized_user_factor[factor] = user_factor[factor]/max[factor]
  return normalized_user_factor

user_factor_max = {'acne':3,'dark_spot':4,'dark_circle':1,'pore':4,'sensitivity':7,'dryness':4,'oily':4,'sun':5,'dust':3}

def calculate_product_point(factors,user_factors,product_df):
  tmp_df = product_df.copy()
  for factor_name in factors:
    col_name_list = list(product_df.filter(regex=factor_name))
    for col_name in col_name_list:
      tmp_df[col_name] = (tmp_df[col_name])*user_factors[factor_name]
  return tmp_df

def euclidian_distance(product_df,product_point_df,ideal_point_df):
    col_name_list = list(product_df.filter(regex='notable')) + list(product_df.filter(regex='cosfunc'))
    tmp = (product_point_df[col_name_list] - ideal_point_df[col_name_list])**2
    matching_score = tmp.sum(axis=1)
    return matching_score

def cosine_similarity(product_df,product_point_df,ideal_point_df):
  col_name_list = list(product_df.filter(regex='notable')) + list(product_df.filter(regex='cosfunc'))
  sqrt_mul_2_product_point = (product_point_df[col_name_list])**2
  sqrt_mul_2_product_point = np.sqrt(sqrt_mul_2_product_point.sum(axis=1))
  sqrt_mul_2_ideal_point_df = (ideal_point_df[col_name_list])**2
  sqrt_mul_2_ideal_point_df = np.sqrt(sqrt_mul_2_ideal_point_df.sum(axis=1))
  tmp = product_point_df[col_name_list]*ideal_point_df[col_name_list]
  denominator = sqrt_mul_2_product_point*sqrt_mul_2_ideal_point_df
  nominator =  tmp.sum(axis=1)
  matching_score = nominator/denominator
  matching_percent = matching_score*100
  return matching_percent

@app.route('/',method = ["POST"])
def product_suggestion():
  #request includes : - user_factors
  #[TODO: ] request handling
  if request.method == "POST":
    content = request.json:
    print(content)
    user_factors = content["user_factors"]
    #[TODO:] check received json
  product_df = pd.read_csv(r"./data/product_score.csv")
  # print(product_df)
  ideal_df = pd.read_csv(r"./data/ideal.csv")
  print(ideal_df)
  norm_user_factors = user_factor_normalization(user_factor_max,user_factors)
  factors = ['acne','dark_spot','dark_circle','pore','sensitivity','dryness','oily','sun','dust']
  product_point_df = calculate_product_point(factors,user_factors,product_df)
  ideal_point_df = calculate_product_point(factors,user_factors,ideal_df)
  product_id = product_point_df['product_id']
  euclidian_dist = euclidian_distance(product_df,product_point_df,ideal_point_df)
  product_distance = pd.concat([product_id, euclidian_dist], axis=1)
  product_distance = product_distance.rename(columns={0: "distance"})
  sorted_product_distance = product_distance.sort_values(by=['distance'])
  cosine_sim = cosine_similarity(product_df,product_point_df,ideal_point_df)
  matching_percent = pd.concat([product_id, cosine_sim], axis=1)
  matching_percent = matching_percent.rename(columns={0: "matching_percent"})
  result = pd.merge(sorted_product_distance, matching_percent, left_on='product_id', right_on='product_id', how='left')
  #[TODO]: prepare result dataframe to response back 
  return "hello"

if __name__ == '__main__':
    app.run()